from django.urls import path
from . import views

urlpatterns = [
    path('', views.taskAPI, name='taskAPI'),
    path('notcompleted', views.taskNotCompleted, name='taskNotCompleted')
]