from django.shortcuts import render, HttpResponse
from django.views.decorators.csrf import csrf_exempt
from rest_framework.parsers import JSONParser
from django.http.response import JsonResponse

from .models import Task
from .serializers import TaskSerializer

@csrf_exempt
def taskAPI(request):
    if request.method == 'GET':
        tasks = Task.objects.all()
        tasks_serializer = TaskSerializer(tasks, many=True)
        return JsonResponse(tasks_serializer.data, safe=False)
    elif request.method == 'POST':
        tasks_data = JSONParser().parse(request)
        tasks_serializer = TaskSerializer(data=tasks_data)
        if tasks_serializer.is_valid():
            tasks_serializer.save()
            return HttpResponse("Added new task")
        return HttpResponse("Failed to add new task. Please make sure the task you provided is valid and contains all necessary info")
    elif request.method == 'PUT':
        tasks_data = JSONParser().parse(request)
        task = Task.objects.get(id=tasks_data['id'])
        tasks_serializer = TaskSerializer(task, data=tasks_data)
        if tasks_serializer.is_valid():
            tasks_serializer.save()
            return HttpResponse("Updated task")
        return HttpResponse("Failed to update task. Please make sure the task you provided is valid and contains all necessary info")
    elif request.method == 'DELETE':
        tasks_data = JSONParser().parse(request)
        task = Task.objects.get(id=tasks_data['id'])
        task.delete()
        return HttpResponse("Task was succesfully deleted")


@csrf_exempt
def taskNotCompleted(request):
    tasks = Task.objects.filter(completed__in=[False])
    tasks_serializer = TaskSerializer(tasks, many=True)
    return JsonResponse(tasks_serializer.data, safe=False)